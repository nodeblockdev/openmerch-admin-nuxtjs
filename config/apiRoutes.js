export default {
  AUTH_LOGIN: "/auth/login",
  AUTH_LOGOUT: "/auth/logout",
  AUTH_REFRESH: "/auth/refresh",
  AUTH_PROFILE: "/auth/user",

  OTP_AUTHENTICATE: "/otp/authenticate",
  OTP_GENERATE: "/otp/generate",
  OTP_TURN_ON: "/otp/turn-on",
  OTP_TURN_OFF: "/otp/turn-off",

  COLLECTION_INDEX: "/contracts",
  UPLOAD: "/media",
  PRODUCTS: "/products",
};
