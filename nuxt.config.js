import apiRoutes from "./config/apiRoutes";

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  env: {
    API_URL: process.env.BASE_URL || "http://localhost:3000",
    BASE_MEDIA: process.env.BASE_MEDIA || "http://localhost:4040/uploads",
    STORE: process.env.STORE || "https://openmerch.io",
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "OpenMerch | Dashboard",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "apple-touch-icon",
        sizes: "180x180",
        href: "/img/apple-touch-icon.png",
      },
      { rel: "icon", sizes: "32x32", href: "/img/favicon-32x32.png" },
      { rel: "icon", sizes: "16x16", href: "/img/favicon-16x16.png" },
      { rel: "manifestmanifest", href: "/img/site.webmanifest" },
      {
        rel: "mask-icon",
        color: "#5bbad5",
        href: "/img/safari-pinned-tab.svg",
      },
      { rel: "preconnect", href: "https://fonts.gstatic.com" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=DM+Sans:wght@700&amp;family=Poppins:wght@400;500;600;700&amp;display=swap",
      },
    ],
    script: [
      { hid: "countdown", src: "/js/lib/jquery.min.js", defer: true },
      {
        hid: "countdown",
        src: "/js/lib/jquery.magnific-popup.min.js",
        defer: true,
      },
      { hid: "stripe", src: "/js/lib/apexcharts.min.js", defer: true },
      { hid: "stripe", src: "/js/lib/svg4everybody.min.js", defer: true },
      { hid: "stripe", src: "/js/lib/owl.carousel.min.js", defer: true },
      { hid: "stripe", src: "/js/head.js", defer: true },
      { hid: "stripe", src: "/js/body.js", defer: true },
      { hid: "stripe", src: "/js/app.js", defer: true },
      { hid: "stripe", src: "/js/charts.js", defer: true },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~/assets/css/app.css", "~/assets/css/style.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/vue-datepicker", ssr: false }, // datepicker plugin here
    "~/plugins/vuelidate",
    "~/plugins/axios",
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/dotenv',
    "@nuxtjs/axios",
    "@nuxtjs/auth-next",
    "@nuxtjs/toast",
    "nuxt-vue-select",
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.BASE_URL || "https://om-back.tombrela.com/api",
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  auth: {
    strategies: {
      local: {
        scheme: 'refresh',
        token: {
          property: "accessToken",
          global: true,
          // required: true,
          // type: 'Bearer'
        },
        refreshToken: {
          property: 'refreshToken',
          data: 'refreshToken',
          maxAge: 60 * 60 * 24 * 30
        },
        user: {
          property: "",
          // autoFetch: true
        },
        endpoints: {
          login: { url: apiRoutes.AUTH_LOGIN, method: "post" },
          logout: { url: apiRoutes.AUTH_LOGOUT, method: "post" },
          user: { url: apiRoutes.AUTH_PROFILE, method: "get" },
        },
      },
    },
  },

  toast: {
    position: "top-center",
    duration: 3000,
  },
};
